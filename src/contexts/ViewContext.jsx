import React, { createContext, useState } from "react";

// Створення контексту
const ViewContext = createContext();

// Провайдер для контексту
const ViewProvider = ({ children }) => {
    const [isTableView, setIsTableView] = useState(false);

    return (
        <ViewContext.Provider value={{ isTableView, setIsTableView }}>
            {children}
        </ViewContext.Provider>
    );
};

export { ViewContext, ViewProvider };
