import React from "react";
import "./TablePage.scss";
import PropTypes from "prop-types";
import ProductListTable from "../../components/ProductListTable/ProductListTable.jsx";

const TablePage = ({ products, handleImgZoomModal }) => {
    return (
        <ProductListTable
          products={products}
          handleImgZoomModal={handleImgZoomModal}
        />
    )
};

export default TablePage;

TablePage.propTypes = {
    products: PropTypes.array,
    handleImgZoomModal: PropTypes.func,
};
