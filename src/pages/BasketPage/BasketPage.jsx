import React from "react";
import { useSelector } from "react-redux";
import { selectTotalProductsInBasket, selectAmountInCart } from '../../store/selectors.js';
import './BasketPage.scss';
import PropTypes from "prop-types";
import ProductList from "../../components/ProductList/ProductList";
import FormBasket from "../../components/Form/FormBasket/FormBasket.jsx";

const BasketPage = ({ handleImgZoomModal }) => {
    const basketList = useSelector(state => state.basketList);
    const totalProductsInBasket = useSelector(selectTotalProductsInBasket);
    const amountInCart = useSelector(selectAmountInCart);

    return (
        <>
            <div className="page__title basketpage__title-container">
                <h2 className="page__title-text">Product in the basket:</h2>
                <p>Total of products: <span>{totalProductsInBasket}pcs.</span></p>
                <p>Amount in cart: <span>{amountInCart}$</span></p>
            </div>
            <div className="basketlist__container">
                <ProductList
                    products={basketList}
                    handleImgZoomModal={handleImgZoomModal}
                />
                <FormBasket />
            </div>
        </>
    )
}

BasketPage.propTypes = {
    handleImgZoomModal: PropTypes.func,
};

export default BasketPage;
