import React from "react";
import './HomePage.scss';
import PropTypes from "prop-types";
import ProductList from "../../components/ProductList/ProductList.jsx";
import { ViewProvider } from "../../contexts/ViewContext.jsx";

const HomePage = ({ products, handleImgZoomModal }) => {
  return (
      <ViewProvider>
        <ProductList
          products={products}
          handleImgZoomModal={handleImgZoomModal}
        />  
      </ViewProvider> 
  )
}

HomePage.propTypes = {
  products: PropTypes.array,
  handleImgZoomModal: PropTypes.func,
};

export default HomePage;


// const HomePage = ({ products, handleImgZoomModal, handleFavoritedList, handleAddBasketModal, setModalContent }) => {
//     return (
//         <ProductList
//           products={products}
//           handleImgZoomModal={handleImgZoomModal}
//           handleFavoritedList={handleFavoritedList}
//           handleAddBasketModal={handleAddBasketModal}
//           setModalContent={setModalContent}
//         />
//     )
// }