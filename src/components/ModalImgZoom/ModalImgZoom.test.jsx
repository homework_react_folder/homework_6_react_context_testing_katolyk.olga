import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import ModalImgZoom from "./ModalImgZoom.jsx";

describe("ModalImgZoom tests", () => {
    // Рендерінг модального вікна з зображенням та назвою
    test("should render the modal with image and name", () => {
        const mockClose = jest.fn();
        const imgCard = "test-image.jpg";
        const nameCard = "Test Product";

        render(<ModalImgZoom close={mockClose} imgCard={imgCard} nameCard={nameCard} />);

        const image = screen.getByAltText(/test product/i);
        const name = screen.getByText(/test product/i);

        expect(image).toBeInTheDocument();
        expect(image).toHaveAttribute("src", imgCard);
        expect(name).toBeInTheDocument();
    });

    // Закриття модального вікна при кліку на кнопку закриття
    test("should close the modal when close button is clicked", () => {
        const mockClose = jest.fn();
        render(<ModalImgZoom close={mockClose} imgCard="test-image.jpg" nameCard="Test Product" />);

        const closeButton = screen.getByRole("button");
        fireEvent.click(closeButton);

        expect(mockClose).toHaveBeenCalledTimes(1);
    });

    // Закриття модального вікна при кліку за межами модального вікна
    test("should close the modal when clicked outside the modal box", () => {
        const mockClose = jest.fn();
        render(<ModalImgZoom close={mockClose} imgCard="test-image.jpg" nameCard="Test Product" />);

        const modalWrapper = screen.getByRole("dialog");
        fireEvent.click(modalWrapper);

        expect(mockClose).toHaveBeenCalledTimes(1);
    });
});