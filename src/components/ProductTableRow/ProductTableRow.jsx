import React from "react";
import { useDispatch } from "react-redux";
import { useLocation } from "react-router-dom"; // Імпортуємо useLocation
import {
    actionToggleAddBasketModal,
    actionSetModalContent,
    actionSetModalText,
    actionAddToFavorited,
    actionRemoveFromBasket,
} from "../../store/slices/slice.js"
import PropTypes from "prop-types";
import "./ProductTableRow.scss";

import Button from "../Button/Button.jsx";
import Zoom from "../../assets/icons/zoom.svg?react";
import Star from "../../assets/icons/star.svg?react";
import Basket from "../../assets/icons/basket.svg?react";
import Close from "../../assets/icons/close_X.svg?react";

const ProductTableRow = ({ product, handleImgZoomModal }) => {
    const dispatch = useDispatch();
    const location = useLocation(); // Використовуємо useLocation для отримання поточного маршруту

    const handleAddBasketModal = () => {
        dispatch(actionSetModalContent(product));
        dispatch(actionSetModalText("Add to basket"));
        dispatch(actionToggleAddBasketModal());
    };

    const handleAddToFavorited = () => {
        dispatch(actionAddToFavorited(product));
    };

    const handleRemoveFromBasket = () => {
        dispatch(actionRemoveFromBasket(product));
    };

    const isBasketPage = location.pathname === "/basketpage"; // Перевіряємо, чи ми на сторінці кошика

    return (

        <tr key={product.article}>
            <td>
                <img
                    src={product.imgCard}
                    alt={product.nameCard}
                    className="product-image"
                    onClick={() => handleImgZoomModal(product)}
                />
            </td>
            <td>{product.nameCard}</td>
            <td>{product.price}$</td>
            <td>{product.article}</td>
            <td>{product.color}</td>
            <td>{product.countInBasket}</td>
            <td>
                <Button onClick={() => handleImgZoomModal(product)}>< Zoom />
                </Button>
                <Button
                    onClick={handleAddToFavorited}
                    className={product.isFavorited === true ? "isFavorited" : ""}
                >< Star />
                </Button>
                <Button onClick={handleAddBasketModal}>
                    < Basket />
                </Button>
                {isBasketPage && (
                    <Button onClick={handleRemoveFromBasket}>
                        < Close />
                    </Button>
                )}
            </td>
        </tr>
    );
};

export default ProductTableRow;

ProductTableRow.propTypes = {
    product: PropTypes.object.isRequired,
    handleImgZoomModal: PropTypes.func.isRequired,
};
