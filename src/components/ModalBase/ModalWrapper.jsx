import React from 'react';
import PropTypes from "prop-types";

const ModalWrapper = ({children, onClick}) => {
    return (
        <div className="modal-wrapper" role="dialog" onClick={(e) => {
            if (e.target === e.currentTarget) {
                onClick();
            }}}>
            {children}
        </div>
    )
};

ModalWrapper.propTypes = {
    children: PropTypes.any,
    onClick: PropTypes.func
};

export default ModalWrapper;
