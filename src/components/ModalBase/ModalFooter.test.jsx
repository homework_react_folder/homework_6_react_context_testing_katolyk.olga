// Снепшот-тести перевірять, чи рендеряться компоненти правильно з наданими пропсами. 
// Якщо ви внесете зміни в компоненти, які впливають на їхній вигляд, тест попередить вас про це. 
// Ви зможете переглянути зміни та вирішити, чи потрібно оновити снепшоти.

import React from 'react';
import renderer from 'react-test-renderer';
import ModalFooter from './ModalFooter';

it('renders correctly with required props', () => {
    const tree = renderer
        .create(<ModalFooter firstText="Add to basket" firstClick={() => {}} />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});

it('renders correctly with all props', () => {
    const tree = renderer
        .create(
            <ModalFooter
                firstText="Add to basket"
                secondaryText="Cancel"
                firstClick={() => {}}
                secondaryClick={() => {}}
            >
                <span>Children Content</span>
            </ModalFooter>
        )
        .toJSON();
    expect(tree).toMatchSnapshot();
});