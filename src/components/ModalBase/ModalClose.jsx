import React from 'react';
import PropTypes from "prop-types";
import CloseX from '../../assets/icons/close_X.svg?react';

const ModalClose = ({onClick}) => {
    return (
        <button type="button" className="modal-close" onClick={onClick}  data-testid="close-button">
            <CloseX/>
        </button>
    )
};

ModalClose.propTypes = {
    onClick: PropTypes.func
};

export default ModalClose;
