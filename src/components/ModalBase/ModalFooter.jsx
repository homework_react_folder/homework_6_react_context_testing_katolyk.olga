import React from 'react';
import PropTypes from "prop-types";
import Button from '../Button/Button';

const ModalFooter = ({ children, firstText, secondaryText, firstClick, secondaryClick }) => {
    
    return (
        <div className="modal-footer">
            
            {children}

            {firstText && (<Button className="button--violet" onClick={firstClick}>
                {firstText}                
            </Button>)}

            {secondaryText && (<Button className="button--white" onClick={secondaryClick}>
                {secondaryText}                
            </Button>)}
            
        </div>
    )
};

ModalFooter.propTypes = {
    children: PropTypes.any,
    firstText: PropTypes.string,
    secondaryText: PropTypes.string,
    firstClick: PropTypes.func,
    secondaryClick: PropTypes.func
};

export default ModalFooter;
