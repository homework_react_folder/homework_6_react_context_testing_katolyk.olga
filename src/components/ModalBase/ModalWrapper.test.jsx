// Снепшот-тести перевірять, чи рендеряться компоненти правильно з наданими пропсами. 
// Якщо ви внесете зміни в компоненти, які впливають на їхній вигляд, тест попередить вас про це. 
// Ви зможете переглянути зміни та вирішити, чи потрібно оновити снепшоти.

import React from 'react';
import renderer from 'react-test-renderer';
import ModalWrapper from './ModalWrapper';

it('renders correctly', () => {
    const tree = renderer
        .create(
            <ModalWrapper onClick={() => {}}>
                <div>Modal Content</div>
            </ModalWrapper>
        )
        .toJSON();
    expect(tree).toMatchSnapshot();
});