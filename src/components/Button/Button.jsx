import React from "react";
import PropTypes from "prop-types";
import cn from "classnames"; // cn - це скорочено classnames, так як в бібліотеці воно головне і по дефолту, то можемо перейменувати, наприклад скоротити
import "./Button.scss";

const Button = (props) => {
    const {
        className,
        type,
        onClick,
        children,
        ...restProps
    } = props;

    return (
        <button
            className={cn("button", className)}
            type={type}
            onClick={onClick}
            {...restProps}
        >
            {children}
        </button >
    )
};

Button.propTypes = {
    className: PropTypes.string,
    type: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.any,
    restProps: PropTypes.object
};

export default Button;


// Button.defaultProps = {
//     type: "button"
// }; // можна прописати дефолт вище в константі через ="", але краще тут окремо в об'єкті дефолтних props

// 2 варіант:
// const Button = ({ className, type, onClick, children, disabled }) => {
//     return (
//         <button className='button btn__first-modal' type="button">Open first modal</button>
//     )
// }


// 3 варіант:
// const Button = (props) => {
//     return (
//         <button className='props.className' type="props.type">Open first modal</button>
//     )
// }