import React from "react";  // бібліотека для работи з компонентами React
import { render, screen, fireEvent } from "@testing-library/react"; // методи з бібліотеки для тестування
import Button from "./Button.jsx";  // компонент для тестування

describe("Button test", () => {
    // перевіряємо, що кнопка рендериться з відповідним текстом.
    test("should render the button with text", ()=> {
        render(<Button>test</Button>);
        expect(screen.getByText(/test/i)).toBeInTheDocument();
    });

    // перевіряємо, що обробник подій (onClick) визивається при кліку на кнопку.
    test("should call onClick handler when clicked", () => {
        const handleClick = jest.fn();
        render(<Button onClick={handleClick}>Click me</Button>);
        fireEvent.click(screen.getByText(/click me/i));
        expect(handleClick).toHaveBeenCalledTimes(1);
    });

    // перевіряємо, що переданний клас додається до класу кнопки.
    test("should apply additional className", () => {
        const { container } = render(<Button className="extra-class">test</Button>);
        expect(container.firstChild).toHaveClass("button");
        expect(container.firstChild).toHaveClass("extra-class");
    });

    // переввряємо передачу типа кнопки (type), що тип кнопки встановлюється правильно.
    test("should set button type", () => {
        render(<Button type="submit">Submit</Button>);
        expect(screen.getByText(/submit/i)).toHaveAttribute("type", "submit");
    });
});


// // describe: Функція від Jest, дозволяє групувати звязані тести для одного компонента.
// // test або it: Функція від Jest для одного теста, приймає опис і саму функцію.
// // render: Рендерить компонент Button з текстом "test" в виртуальний DOM-дерево для тестування.
// // expect(...).toBeInTheDocument(): перевірка, що знайдений елемент присутній в документі.
// // screen - это глобальний обєкт від @testing-library/react, дозволяє шукати елементи в рендереному віртуальному DOM.
// // screen.getByText(/test/i): використовує регулярний вираз для пошука елемента, який має текст "test". 
// // Параметр /test/i означає, що пошук буде нечуттєвий до регістру


// // варіант, що використовує значення локальної змінної, яку повертає функція render
// import React from "react";
// import { render } from "@testing-library/react";
// import Button from "./Button.jsx";

// describe("Button test", () => {
//     test("should render the button with text", ()=> {
//         const button = render(<Button>test</Button>)
//         expect(button.getByText(/test/i)).toBeIntheDocument();        
//     });
// })