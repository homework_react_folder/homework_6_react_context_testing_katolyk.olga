import React from "react";
import PropTypes from "prop-types";
import "./ProductListTable.scss";
import ProductTableRow from "../ProductTableRow/ProductTableRow.jsx";

const ProductListTable = ({ products, handleImgZoomModal }) => {    
    return (
        <table className="product-list__table">
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Article</th>
                    <th>Color</th>
                    <th>countInBasket</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {products.map(product => (
                   <ProductTableRow 
                        key={product.article}
                        product={product}
                        handleImgZoomModal={handleImgZoomModal}
                   />
                ))}
            </tbody>
        </table>
    );
};

export default ProductListTable;

ProductListTable.propTypes = {
    products: PropTypes.array.isRequired,
    handleImgZoomModal: PropTypes.func.isRequired,
};
