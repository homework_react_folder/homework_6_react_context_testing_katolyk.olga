import React from "react";
import PropTypes from "prop-types";
import Card from "../Card/Card.jsx";
import "./ProductListCard.scss";

const ProductListCard = ({ products, handleImgZoomModal }) => {
    return (
        <div className="product-list__cards">
            {products.map(product => (
                <Card
                    key={product.article}
                    product={product}
                    handleImgZoomModal={handleImgZoomModal}
                />
            ))}
        </div>
    )
};

export default ProductListCard;

ProductListCard.propTypes = {
    products: PropTypes.array,
    handleImgZoomModal: PropTypes.func,
};
