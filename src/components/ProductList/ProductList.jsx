import React, { useContext } from "react";
import PropTypes from "prop-types";
import ProductListTable from "../ProductListTable/ProductListTable.jsx";
import ProductListCard from "../ProductListCard/ProductListCard.jsx";
import { ViewContext } from "../../contexts/ViewContext.jsx";
import "./ProductList.scss";

const ProductList = ({ products, handleImgZoomModal }) => {
    const { isTableView, setIsTableView } = useContext(ViewContext);

    const handleToggleView = () => {
        setIsTableView(prev => !prev);
    };

    return (
        <>
            <div className="product-list__header">                
                <input type="checkbox" id="ch" onChange={handleToggleView} checked={isTableView} />
                {/* <span></span> */}
                    <label htmlFor="ch" className="ch__list-toggle"></label>
            </div>
            <div>
                {isTableView ? (
                    <ProductListTable
                        products={products}
                        handleImgZoomModal={handleImgZoomModal}
                    />
                ) : (
                    <ProductListCard                        
                        products={products}
                        handleImgZoomModal={handleImgZoomModal}
                    />                    
                )}
            </div>
        </>

    );
};

export default ProductList;

ProductList.propTypes = {
    products: PropTypes.array,
    handleImgZoomModal: PropTypes.func,
};
