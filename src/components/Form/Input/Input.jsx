import React, { useRef, useEffect } from "react";
import { Field, ErrorMessage } from "formik";
import cn from "classnames";
import PropTypes from "prop-types";
// import InputMask from "react-input-mask";
import MaskedInput from "react-text-mask";
import "./Input.scss";

export const Input = (props) => {
    const {
        className,
        type = "text",
        placeholder,
        name,
        label,
        error,
        mask,
        options,
        ...restProps
    } = props;

    // const inputRef = useRef(null);

    // useEffect(() => {
    //     if(mask && inputRef.current) {
    //         // Використовуйте реф для отримання доступу до елемента
    //         // Можна виконувати дії з inputRef, якщо потрібно
    //     }
    // }, [mask]);

    return (
        <>
            <label className={cn("form-item", className, { "has-validation": error })}>
                <div className="input-container">
                    <p className="form-label">{label}</p>
                    {type === "select" ? (
                        <Field as="select" className="select-placeholder" name={name} {...restProps}>
                            <option value="" disabled>{placeholder}</option>
                            {options && options.map((option, index) => (
                                <option key={index} value={option.value}>
                                    {option.label}
                                </option>
                            ))}
                        </Field>
                    ) : mask ? (
                        <Field name={name}>
                            {({ field }) => (
                                <MaskedInput
                                    {...field}
                                    mask={mask}
                                    placeholder={placeholder}
                                    type={type}
                                    className="form-control"
                                />
                            )}
                        </Field>
                    ) : (
                        <Field
                            type={type}
                            name={name}
                            placeholder={placeholder}
                            {...restProps}
                        />
                    )}
                </div>
                <ErrorMessage name={name} className="error-message" component={"p"} />
            </label>
        </>
    )
};

// Input.defaultProps = {
//     type: "text",
// }; // застарілий метод виносу дефолтних зеачень

Input.propTypes = {
    className: PropTypes.string,
    type: PropTypes.string,
    placeholder: PropTypes.string,
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    error: PropTypes.bool,
    mask: PropTypes.oneOfType([
        PropTypes.array,
        PropTypes.func
    ]),
    options: PropTypes.arrayOf(PropTypes.shape({
        value: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired,
    })),
    restProps: PropTypes.object,
};
