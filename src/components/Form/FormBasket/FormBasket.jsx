import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom"
import { Form, Formik } from "formik";
import * as Yup from "yup";

import { useSelector, useDispatch } from "react-redux";
import { selectorFormValues, selectorOrderValues, selectorBasketList, selectTotalProductsInBasket, selectAmountInCart } from "../../../store/selectors.js";
import { actionUpdateOrderValues, actionAddOrder, actionClearBasket } from "../../../store/slices/slice.js";

import { Input } from "../Input/Input.jsx";
import Button from "../../Button/Button.jsx";
import "./FormBasket.scss";

export const FormBasket = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const formValues = useSelector(selectorFormValues);
    // const orderValues = useSelector(selectorOrderValues);
    const basketList = useSelector(selectorBasketList);
    const totalProductsInBasket = useSelector(selectTotalProductsInBasket);
    const amountInCart = useSelector(selectAmountInCart);

    // useEffect(() => {
        // console.log("Form values from Redux:", formValues);
        // console.log("Order values from Redux: ", orderValues);
    // }, [formValues, orderValues]); // Це має показувати актуальні значення з Redux при кожній зміні

    const handleSubmit = (values, { resetForm }) => {
        if (basketList.length === 0) {
            alert("Your basket is empty. Please add products to the basket before placing an order.");
            return;
        }

        const orderWithProducts = {
            ...values,
            productsInOrder: basketList,
            numberOrder: Date.now(), // додаємо номер замовлення
            totalProductsInBasket: totalProductsInBasket,
            amountInCart: amountInCart,
        };
        // console.log("Order to be dispatched:", orderWithProducts); // Додаткове логування
        dispatch(actionUpdateOrderValues(orderWithProducts));
        dispatch(actionAddOrder(orderWithProducts)); // додаємо замовлення до масиву orders
        dispatch(actionClearBasket()); // очищуємо кошик після додавання замовлення
        console.log("Your order (values in Form): ", values);
        console.log("Your order (order with Products): ", orderWithProducts);
        resetForm();
        setTimeout(() => {
            navigate('/orders');
        }, 1000)
    };

    const phoneRegExp = /^(\+?\d{1,4}[\s-]?)?((\(\d{1,5}\))|\d{1,5})[\s-]?\d{1,5}([\s-]?\d{1,5}){1,2}$/;
    const nameRegExp = /^(?!\s)(?!.*\s$)(?!.*\s\s)[a-zA-Zа-яА-ЯёЁіІїЇєЄ0-9\s\-']+$/;

    const validationSchema = Yup.object({
        name: Yup.string()
            .matches(nameRegExp, 'Use letters (Cyrillic and Latin), numbers, without extra spaces')
            .min(2, 'Too Short!')
            .max(50, 'Too Long!')
            .required('Required'),
        phone: Yup.string()
            .matches(phoneRegExp, 'Invalid phone number')
            .required('Required'),
        email: Yup.string().email('Invalid email address').required('Required'),
        city: Yup.string()
            .required('Required'),
        delivery: Yup.string()
            .required('Required'),
        comments: Yup.string()
            .max(200, 'Too Long!'),
    });

    const initialValues = {
        ...formValues,
        phone: formValues.phone || '+38'
    };

    return (
        <div>
            <Formik
                validationSchema={validationSchema}
                // initialValues={formValues}
                initialValues={initialValues}
                onSubmit={handleSubmit}
            >
                {({ errors, touched }) => (
                    <Form>
                        <fieldset className="form-block">
                            <legend>To order</legend>
                            <Input name="name" placeholder="name" label={"Name *"} error={errors.name && touched.name} />
                            {/* <Input name="phone" placeholder="phone" label={"Phone *"} error={errors.phone && touched.phone} mask={"+38(099)999-99-99"}/> */}
                            <Input name="phone" placeholder="phone" label={"Phone *"} error={errors.phone && touched.phone} mask={['+', '3', '8', '(', /\d/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/]} />
                            <Input name="email" placeholder="email" label={"Email *"} error={errors.email && touched.email} />
                            <Input 
                                name="city" 
                                placeholder="Choose city" 
                                label={"City *"} 
                                type="select"
                                options={[
                                    { value: "Kyiv", label: "Kyiv"},
                                    { value: "Lviv,", label: "Lviv,"},
                                    { value: "Odessa", label: "Odessa" },
                                    { value: "Kharkiv", label: "Kharkiv" },
                                ]}
                                error={errors.city && touched.city} />
                            <Input 
                                name="delivery" 
                                placeholder="Choose delivery method" 
                                label={"Delivery *"} 
                                type="select"
                                options={[
                                    { value: "NovaPoshta", label: "Nova Poshta"},
                                    { value: "UkrPoshta", label: "Ukr Poshta"},
                                    { value: 'courier', label: 'Courier' },
                                    { value: 'pickup', label: 'Pickup' },
                                ]}
                                error={errors.delivery && touched.delivery} />
                            <Input name="comments" placeholder="comments" label={"Comments"} error={errors.comments && touched.comments} />
                            <Button type={"submit"}>Create order</Button>
                        </fieldset>
                    </Form>
                )}
            </Formik>
        </div>
    )
};

export default FormBasket;



// const nameRegExp = /^(?!\s)(?!.*\s$)(?!.*\s\s)[a-zA-Zа-яА-ЯёЁіІїЇєЄ0-9\s\-']+$/;
// Пояснення регулярного виразу:
// ^(?!\s) - забороняє пробіл на початку рядка.
// (?!.*\s$) - забороняє пробіл в кінці рядка.
// (?!.*\s\s) - забороняє більше одного пробілу підряд.
// [a-zA-Zа-яА-ЯёЁіІїЇєЄ0-9\s\-']+$ - дозволяє літери (великі і малі кирилиця і латиниця), цифри, тире, апострофи та пробіли.
