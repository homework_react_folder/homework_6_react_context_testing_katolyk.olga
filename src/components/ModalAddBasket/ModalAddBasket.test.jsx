import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import ModalAddBasket from "./ModalAddBasket.jsx";
import { actionAddToBasket } from "../../store/slices/slice.js";

const mockStore = configureStore([]);

describe("ModalAddBasket tests", () => {
    // // Рендеринг модального вікна з зображенням і інформацією о товарі
    let store;
    const mockClose = jest.fn();

    beforeEach(() => {
        store = mockStore({
            modalContent: {
                imgCard: "test-image.jpg",
                nameCard: "Test Product",
                price: 100,
                article: "12345",
                color: "red",
                modalText: "Add to basket"
            },
            basketList: []
        });

        store.dispatch = jest.fn();
        jest.clearAllMocks(); // Очистка всіх моків перед кожним тестом
    });

    // // Додавання товару в корзину при кліку на кнопку додавання товару
    test("should add product to basket when add button is clicked", () => {
        render(
            <Provider store={store}>
                <ModalAddBasket close={mockClose} />
            </Provider>
        );

        const addButton = screen.getByText(/add to basket/i);
        fireEvent.click(addButton);

        expect(store.dispatch).toHaveBeenCalledWith(actionAddToBasket(store.getState().modalContent));
    });

    // // Закриття модального вікна при кліку на кнопку додавання товару при додаванні товару
    test("should close the modal when add button is clicked", () => {
        render(
            <Provider store={store}>
                <ModalAddBasket close={mockClose} />
            </Provider>
        );

        const addButton = screen.getByText(/add to basket/i);
        fireEvent.click(addButton);

        expect(mockClose).toHaveBeenCalledTimes(1);
    });

    // // Закриття модального вікна при кліку за його межами
    test("should close the modal when clicking outside the modal box", () => {
        render(
            <Provider store={store}>
                <ModalAddBasket close={mockClose} />
            </Provider>
        );

        const modalWrapper = screen.getByRole("dialog");
        fireEvent.click(modalWrapper);

        expect(mockClose).toHaveBeenCalledTimes(1);
    });

    // // Закриття модального вікна при кліку на кнопку закриття
    test("should close the modal when close button is clicked", () => {
        render(
            <Provider store={store}>
                <ModalAddBasket close={mockClose} />
            </Provider>
        );

        const closeButton = screen.getByTestId("close-button");
        fireEvent.click(closeButton);

        expect(mockClose).toHaveBeenCalledTimes(1);
    });
});
