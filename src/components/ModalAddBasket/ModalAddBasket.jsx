import React from "react";
import PropTypes from "prop-types";
import "../ModalBase/ModalBase.scss";
import "./ModalAddBasket.scss";
import ModalWrapper from "../ModalBase/ModalWrapper.jsx";
import ModalBox from "../ModalBase/ModalBox.jsx";
import ModalClose from "../ModalBase/ModalClose.jsx";
import ModalHeader from "../ModalBase/ModalHeader.jsx"
import ModalBody from "../ModalBase/ModalBody.jsx";
import ModalFooter from "../ModalBase/ModalFooter.jsx";
import Basket from "../../assets/icons/basket.svg?react";
import { useSelector, useDispatch } from "react-redux";
import { actionAddToBasket } from "../../store/slices/slice.js"

const ModalAddBasket = ({ close }) => {
    const dispatch = useDispatch();

    const modalContent = useSelector(state => state.modalContent);

    const {imgCard, nameCard, price, article, color, modalText} = modalContent;

    const handleAddToBasket = () => {
        dispatch(actionAddToBasket(modalContent));
        close();
    }

    return (
        <ModalWrapper onClick={close}>
            <ModalBox>
                <ModalClose onClick={close}/>
                <img src={imgCard} className="modal-header__img" alt={nameCard} />
                <ModalHeader>{nameCard}</ModalHeader>
                <ModalBody>
                    Good choice: {color} toy,<br/>
                    article: {article}, price: {price}$
                </ModalBody>
                <ModalFooter firstText={modalText} firstClick={handleAddToBasket}>
                    < Basket />
                </ModalFooter>
            </ModalBox>
        </ModalWrapper>
    )
};

ModalAddBasket.propTypes = {
    close: PropTypes.func,
};

export default ModalAddBasket;
