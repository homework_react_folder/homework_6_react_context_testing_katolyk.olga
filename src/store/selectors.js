// selectors — це функції, які використовуються для витягання даних зі стану (state). 
// Вони не змінюють стан, а тільки отримують певну інформацію з нього, тому ми їх НЕ вставляємо в reducers

export const selectorFormValues = (state) => state.formValues;
export const selectorOrderValues = (state) => state.orderValues;
export const selectorProducts = (state) => state.products;
export const selectorBasketList = (state) => state.basketList;
export const selectorOrders = (state) => state.orders;

export const selectTotalProductsInBasket = (state) => {
    return state.basketList.reduce((total, item) => total + item.countInBasket, 0);
};

export const selectAmountInCart = (state) => {
    return state.basketList.reduce((total, item) => total + item.price*item.countInBasket, 0);
}
