// Middleware у Redux - це функції, які стоять між дією (action) і ред'юсером (reducer). 
// Вони дозволяють перехоплювати або змінювати дії перед тим, як вони дійдуть до ред'юсера. 
// Це корисно для таких речей, як логування, обробка асинхронних запитів, та інші побічні ефекти.

// Як працює Middleware?
// Коли ви диспатчите (викликаєте) дію, вона спочатку проходить через всі підключені middleware, 
// перш ніж досягне ред'юсера. 

// Кожен middleware має можливість:
// - Перехопити дію і зробити з нею щось (наприклад, логувати її).
// - Змінити дію.
// - Відмовитися від дії (пропустити її).
// - Продовжити дію до наступного middleware або ред'юсера.
// Чи потрібна додаткова бібліотека?
// Redux поставляється з основними middleware (як redux-thunk для обробки асинхронних дій), 
// але створення власного middleware не потребує додаткових бібліотек.
// Middleware дозволяє вам впроваджувати логіку, яка працює з діями перед тим, 
// як вони досягнуть ред'юсера. 

const localStorageMiddleware = store => next => action => {
    // Викликаємо наступний middleware або ред'юсер, 
    // додаємо умови для збереження стану в localStorage при будь-якій зміні в масивах
    const result = next(action);
    const state = store.getState();

    switch (action.type) {
        case "root/actionAddToBasket":
        case "root/actionRemoveFromBasket":
        case "root/actionAddToFavorited":
        case "root/actionUpdateProducts":
        case "root/actionSyncProductsWithFavoritedAndBasket":
        case "root/actionSyncFavoritedWithBasket":
        case "root/actionSyncBasketWithFavorited":
        case "root/actionUpdateOrderValues":
        case "root/actionAddOrder":
        case "root/actionSetOrders":
            localStorage.setItem("basketList", JSON.stringify(state.basketList));
            localStorage.setItem("mainProductList", JSON.stringify(state.products));
            localStorage.setItem("favoritedList", JSON.stringify(state.favoritedList));
            localStorage.setItem("orderLast", JSON.stringify(state.orderValues));
            localStorage.setItem("ordersList", JSON.stringify(state.orders));
            break;
        default:
            break;
    }

    return result;
};

export default localStorageMiddleware;

// Другий спосіб, з використання типів actions:
// const localStorageMiddleware = store => next => action => {
//     // Викликаємо наступний middleware або ред'юсер, додаємо умови для збереження стану при будь-якій зміні в масивах
//     const result = next(action);
    
//     // Слідкуємо за діями, які змінюють basketList, favoritedList і products, перевіряємо типи дій, які нас цікавлять
//     if (action.type === 'root/actionAddToBasket' || action.type === 'root/actionRemoveFromBasket') {
//         const basketList = store.getState().basketList;
//         localStorage.setItem('basketList', JSON.stringify(basketList));

//         const products = store.getState().products;
//         localStorage.setItem('mainProductList', JSON.stringify(products));
//     }

//     if (action.type === 'root/actionFetchProducts/fulfilled' || action.type === 'root/actionUpdateProducts') {
//         const products = store.getState().products;
//         localStorage.setItem('mainProductList', JSON.stringify(products));
//     }

//     if (action.type === 'root/actionAddToFavorited') {
//         const favoritedList = store.getState().favoritedList;
//         localStorage.setItem('favoritedList', JSON.stringify(favoritedList));

//         const products = store.getState().products;
//         localStorage.setItem('mainProductList', JSON.stringify(products));
//     }

//     return result;
// };

// export default localStorageMiddleware;










// try {
//     // Зберігаємо тільки необхідні частини стану
//     const serializableState = {
//         basketList: state.basketList,
//         products: state.products,
//         favoritedList: state.favoritedList,
//         orderValues: state.orderValues,
//         orders: state.orders,
//     };

//     localStorage.setItem("basketList", JSON.stringify(serializableState.basketList));
//     localStorage.setItem("mainProductList", JSON.stringify(serializableState.products));
//     localStorage.setItem("favoritedList", JSON.stringify(serializableState.favoritedList));
//     localStorage.setItem("orderLast", JSON.stringify(serializableState.orderValues));
//     localStorage.setItem("ordersList", JSON.stringify(serializableState.orders));
// } catch (err) {
//     console.error("Failed to save state to localStorage:", err);
// }