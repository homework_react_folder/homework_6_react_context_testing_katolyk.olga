import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const initialState = {
	products: [],
    isAddBasketModal: false,
    modalContent: {
        imgCard: undefined,
        modalText: "Add to basket",
        nameCard: undefined,
        price: undefined,
        article: undefined,
        color: undefined,
        isFavorited: undefined,
        countInBasket: undefined,
    },
    favoritedList: [],
    basketList: [],  
    formValues: {
        name: "",
        phone: "",
        email: "",
        city: "",
        delivery: "",
        comments: "",
    },
    orderValues: {
        numberOrder: "",
        name: "",
        phone: "",
        email: "",
        city: "",
        delivery: "",
        comments: "",
        productsInOrder: [],
        totalProductsInBasket: "",
        amountInCart: "",
    },
    orders: [],
};

export const actionFetchProducts = createAsyncThunk(
    "products/fetchProducts",
    async () => {
        const response = await fetch("../../../asets/products.json");
        const data = await response.json();
        return data;
    }
);

const rootSlice = createSlice({
    name: "root",  // назва нашого ред'юсера
    initialState,  // інішнл стейт це базові значення для нашого додатка, коротка форма запису "ключ: значення," коли ключ і значення - однакові

    // reducers у createSlice використовуються для визначення функцій, які змінюють стан (state) у вашому Redux store. 
    // Кожна функція в reducers описує, як стан повинен змінюватися у відповідь на певну дію (action).     
    reducers: {
        actionToggleAddBasketModal: (state) => {
            state.isAddBasketModal = !state.isAddBasketModal;
        },

        actionSetModalContent: (state, action) => {
            state.modalContent = action.payload;
        },

        actionSetModalText: (state, action) => { // дія для зміни modalText на кнопці
            state.modalContent.modalText = action.payload;
        },

        actionAddToFavorited: (state, action) => {
            const item = action.payload;

            const existingItem = state.favoritedList.find(product => product.article === item.article);
            if(existingItem) {
                existingItem.isFavorited = !existingItem.isFavorited;
                state.favoritedList = state.favoritedList.filter(product => product.article !== item.article);
            } else {
                state.favoritedList.push({...item, isFavorited: true});
            }            
        },

        actionAddToBasket: (state, action) => {
            const item = action.payload;
            const existingItem = state.basketList.find(product => product.article === item.article);
            if(existingItem) {
                existingItem.countInBasket += 1;
            } else {
                state.basketList.push({...item, countInBasket: 1});
            }             
        },

        actionRemoveFromBasket: (state, action) => {
            const item = action.payload;
            state.basketList = state.basketList.filter(product => product.article !== item.article);
        },

        actionUpdateProducts: (state, action) => {
            state.products = action.payload;
        },
        actionSyncProductsWithFavoritedAndBasket: (state) => {
            state.products.forEach(product => {
              const basketItem = state.basketList.find(item => item.article === product.article);
              const favoritedItem = state.favoritedList.find(item => item.article === product.article);
              if (basketItem) {
                product.countInBasket = basketItem.countInBasket;
              } else {
                product.countInBasket = 0;
              }
              product.isFavorited = favoritedItem ? true : false;
            });
          },
        actionSyncFavoritedWithBasket: (state) => {
            state.favoritedList.forEach(favorite => {
                const basketItem = state.basketList.find(item => item.article === favorite.article);
                favorite.countInBasket = basketItem ? basketItem.countInBasket : 0;
            });
        },
        actionSyncBasketWithFavorited: (state) => {
            state.basketList.forEach(basket => {
                const favoritedItem = state.favoritedList.find(item => item.article === basket.article);
                basket.isFavorited = favoritedItem ? true : false;
            });
        },

        actionUpdateOrderValues: (state, {payload}) => {
            state.orderValues = payload;
        },

        actionAddOrder: (state, action) => {
            console.log("Adding order:", action.payload);
            // console.log("Current state orders:", state.orders);
            // state.orders.push(action.payload);
            state.orders.unshift(action.payload);
            // console.log("Updated state orders:", state.orders);            
        },
        actionSetOrders: (state, action) => {
            state.orders = action.payload;
        },
        actionClearBasket: (state) => {
            state.basketList = [];
        },
    },
    extraReducers: (builder) => {
            // builder.addCase(actionFetchProducts.fulfilled, (state, { payload }) => {
            //     state.products = payload;
            // })    
            builder.addCase(actionFetchProducts.fulfilled, (state, action) => {
                    state.products = action.payload;
                })                
        }    
});

export const { 
    actionToggleAddBasketModal, 
    actionSetModalContent, 
    actionSetModalText, 
    actionAddToFavorited,
    actionAddToBasket,
    actionUpdateProducts,
    actionRemoveFromBasket,
    actionSyncProductsWithFavoritedAndBasket,
    actionSyncFavoritedWithBasket,
    actionSyncBasketWithFavorited,
    actionUpdateOrderValues,
    actionAddOrder,
    actionSetOrders,
    actionClearBasket,
} = rootSlice.actions;

export default rootSlice.reducer; // і потім генеруємо сам редюсер, який підключаємо в нашем configureStore
