// Для написання тестів для редьюсера, скористаємося Jest.

// Структура тестів, що перевіряють основні функціональні можливості редьюсера
// Перевірка початкового стану
// Перевірка actionToggleAddBasketModal
// Перевірка actionSetModalContent
// Перевірка actionAddToBasket
// Перевірка actionRemoveFromBasket

import rootReducer, { 
    actionToggleAddBasketModal, 
    actionSetModalContent, 
    actionAddToBasket, 
    actionRemoveFromBasket,
    actionUpdateProducts
} from './slice.js';

const initialState = {
    products: [],
    isAddBasketModal: false,
    modalContent: {
        imgCard: undefined,
        modalText: "Add to basket",
        nameCard: undefined,
        price: undefined,
        article: undefined,
        color: undefined,
        isFavorited: undefined,
        countInBasket: undefined,
    },
    favoritedList: [],
    basketList: [],
    formValues: {
        name: "",
        phone: "",
        email: "",
        city: "",
        delivery: "",
        comments: "",
    },
    orderValues: {
        numberOrder: "",
        name: "",
        phone: "",
        email: "",
        city: "",
        delivery: "",
        comments: "",
        productsInOrder: [],
        totalProductsInBasket: "",
        amountInCart: "",
    },
    orders: [],
};

describe('rootReducer', () => {
    // Перевірка початкового стану: перевіряємо, 
    // чи повертає редьюсер правильний початковий стан, коли йому не передають жодних дій.
    it('should return the initial state', () => {
        expect(rootReducer(undefined, {})).toEqual(initialState);
    });

    // Перевірка дій: Для кожної дії створюємо попередній стан, 
    // застосовуємо дію і перевіряємо, чи повертається очікуваний новий стан
    it('should handle actionToggleAddBasketModal', () => {
        const previousState = { ...initialState, isAddBasketModal: false };
        expect(rootReducer(previousState, actionToggleAddBasketModal())).toEqual({
            ...initialState,
            isAddBasketModal: true,
        });
    });

    it('should handle actionSetModalContent', () => {
        const modalContent = {
            imgCard: "test-image.jpg",
            nameCard: "Test Product",
            price: 100,
            article: "12345",
            color: "red",
            modalText: "Add to basket"
        };
        expect(rootReducer(initialState, actionSetModalContent(modalContent))).toEqual({
            ...initialState,
            modalContent
        });
    });

    it('should handle actionAddToBasket', () => {
        const product = {
            imgCard: "test-image.jpg",
            nameCard: "Test Product",
            price: 100,
            article: "12345",
            color: "red",
            modalText: "Add to basket"
        };
        const previousState = { ...initialState, basketList: [] };
        expect(rootReducer(previousState, actionAddToBasket(product))).toEqual({
            ...initialState,
            basketList: [{ ...product, countInBasket: 1 }],
        });
    });

    it('should handle actionRemoveFromBasket', () => {
        const product = {
            imgCard: "test-image.jpg",
            nameCard: "Test Product",
            price: 100,
            article: "12345",
            color: "red",
            modalText: "Add to basket"
        };
        const previousState = { ...initialState, basketList: [{ ...product, countInBasket: 1 }] };
        expect(rootReducer(previousState, actionRemoveFromBasket(product))).toEqual({
            ...initialState,
            basketList: [],
        });
    });

    it('should handle actionUpdateProducts', () => {
        const products = [
            { article: "12345", nameCard: "Test Product 1" },
            { article: "67890", nameCard: "Test Product 2" }
        ];
        expect(rootReducer(initialState, actionUpdateProducts(products))).toEqual({
            ...initialState,
            products
        });
    });
});
