// конфігуруємо наш store для підключення slice.
import { configureStore } from "@reduxjs/toolkit";
import rootReducer from "./slices/slice.js"; // nameSlice1, nameSlice2, nameSlice3, якщо ред'юсерів декілька, декілька сутностей
import localStorageMiddleware from "./middleware/localStorageMiddleware.js";

// Функція завантаження стану з Local Storage
const loadState = () => {
    try {
        const serializedBasketList = localStorage.getItem('basketList');
        const serializedProducts = localStorage.getItem('mainProductList');
        const serializedFavoritedList = localStorage.getItem('favoritedList');
        const serializedOrders = localStorage.getItem('orders'); // Додаємо orders

        return {
            basketList: serializedBasketList ? JSON.parse(serializedBasketList) : undefined,
            products: serializedProducts ? JSON.parse(serializedProducts) : undefined,
            favoritedList: serializedFavoritedList ? JSON.parse(serializedFavoritedList) : undefined,
            orders: serializedOrders ? JSON.parse(serializedOrders) : [], // Ініціалізуємо orders
        };
    } catch (err) {
        return undefined;
    }
};

// Початковий стан слайсу
const initialState = {
    products: [],
    isAddBasketModal: false,
    modalContent: {
        imgCard: undefined,
        modalText: "Add to basket",
        nameCard: undefined,
        price: undefined,
        article: undefined,
        color: undefined,
        isFavorited: undefined,
        countInBasket: undefined,
    },
    favoritedList: [],
    basketList: [],
    formValues: {
        name: "",
        phone: "",
        email: "",
        city: "",
        delivery: "",
        comments: "",
    },
    orderValues: {
        numberOrder: "",
        name: "",
        phone: "",
        email: "",
        city: "",
        delivery: "",
        comments: "",
        productsInOrder: [],
    },
    orders: [], // Додаємо orders до початкового стану
};

const preloadedStateWithLocalStorage = loadState();

// Об'єднання preloadedStateWithLocalStorage з initialState
const combinedInitialState = {
    ...initialState,
    ...preloadedStateWithLocalStorage,
    basketList: preloadedStateWithLocalStorage.basketList || initialState.basketList,
    products: preloadedStateWithLocalStorage.products || initialState.products,
    favoritedList: preloadedStateWithLocalStorage.favoritedList || initialState.favoritedList,
    orders: preloadedStateWithLocalStorage.orders || initialState.orders, // Ініціалізуємо orders
};

const preloadedState = loadState();
console.log("Preloaded state:", preloadedState); // Додаткове логування

const store = configureStore({
    reducer: rootReducer,
    preloadedState: combinedInitialState,  // Використовуємо комбінований початковий стан
    middleware: (getDefaultMiddleware) => 
        getDefaultMiddleware().concat(localStorageMiddleware),
});

export default store;















// const loadState = () => {
//     try {
//         const serializedBasketList = localStorage.getItem('basketList');
//         const serializedProducts = localStorage.getItem('mainProductList');
//         const serializedFavoritedList = localStorage.getItem('favoritedList');

//         return {
//             basketList: serializedBasketList ? JSON.parse(serializedBasketList) : [],
//             products: serializedProducts ? JSON.parse(serializedProducts) : [],
//             favoritedList: serializedFavoritedList ? JSON.parse(serializedFavoritedList) : [],
//         };
//     } catch (err) {
//         return undefined;
//     }
// };

// const preloadedStateWithLocalStorage = loadState();

// const store = configureStore({
//     reducer: rootReducer,
//     preloadedStateWithLocalStorage,  // Додаємо стан, завантажений з Local Storage
//     middleware: (getDefaultMiddleware) => 
//         getDefaultMiddleware().concat(localStorageMiddleware),
// });

// export default store;
