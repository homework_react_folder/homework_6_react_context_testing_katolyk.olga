import React, { useState, useEffect } from 'react';
import { Routes, Route } from 'react-router-dom';

import ModalImgZoom from './components/ModalImgZoom/ModalImgZoom.jsx';
import ModalAddBasket from './components/ModalAddBasket/ModalAddBasket.jsx';
import Header from "./components/Header/Header.jsx";
import HomePage from './pages/HomePage/HomePage.jsx';
import FavoritePage from './pages/FavoritePage/FavoritePage.jsx';
import BasketPage from './pages/BasketPage/BasketPage.jsx';
import OrdersPage from './pages/OrdersPage/OrdersPage.jsx';
import TablePage from './pages/TablePage/TablePage.jsx';
import { useDispatch, useSelector } from "react-redux";
import { 
  actionFetchProducts, 
  actionToggleAddBasketModal, 
  actionUpdateProducts, 
  actionSyncProductsWithFavoritedAndBasket, 
  actionSyncBasketWithFavorited, 
  actionSyncFavoritedWithBasket,
  actionSetOrders
} from "./store/slices/slice.js";
import { ViewProvider } from "./contexts/ViewContext"; // Імпортуємо провайдер контексту

const App = () => {
  const dispatch = useDispatch();
  const products = useSelector(state => state.products);
  const basketList = useSelector(state => state.basketList);
  const favoritedList = useSelector(state => state.favoritedList);
  const isAddBasketModal = useSelector(state => state.isAddBasketModal);

  const [isImgZoomModal, setIsImgZoomModal] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null); // стан для збереження обраного продукту

  useEffect(() => {
    const storedProducts = localStorage.getItem('mainProductList');
    const storedOrders = localStorage.getItem('ordersList');

    if (storedProducts) {
      dispatch(actionUpdateProducts(JSON.parse(storedProducts)));
    } else {
      dispatch(actionFetchProducts())
        .then((response) => {
          if (response.payload) {
            dispatch(actionUpdateProducts(response.payload));
          }
        });
    }

    if (storedOrders) {
      dispatch(actionSetOrders(JSON.parse(storedOrders)));
    }
  }, [dispatch]);

  useEffect(() => {
    dispatch(actionSyncProductsWithFavoritedAndBasket());
  }, [basketList, favoritedList, dispatch]);

  useEffect(() => {
    dispatch(actionSyncFavoritedWithBasket());
  }, [basketList, dispatch]);

  useEffect(() => {
    dispatch(actionSyncBasketWithFavorited());
  }, [favoritedList, dispatch]);

  const handleImgZoomModal = (product) => {
    setSelectedProduct(product)
    setIsImgZoomModal((prevState) => !prevState);
  }

  return (
    <ViewProvider>
      <Header />
      <main>
        <Routes>
          <Route path="/" element={<HomePage
            products={products}
            handleImgZoomModal={handleImgZoomModal}
          />} />
          <Route path="/favoritepage" element={<FavoritePage handleImgZoomModal={handleImgZoomModal} />} />
          <Route path="/basketpage" element={<BasketPage handleImgZoomModal={handleImgZoomModal} />} />
          <Route path="/orders" element={<OrdersPage />} />
          <Route path="/table" element={<TablePage
            products={products}
            handleImgZoomModal={handleImgZoomModal}
          />} />
        </Routes>
      </main>
      {isImgZoomModal && <ModalImgZoom
        close={handleImgZoomModal}
        imgCard={selectedProduct.imgCard}
        nameCard={selectedProduct.nameCard}
      />}
      {isAddBasketModal && <ModalAddBasket
        close={() => dispatch(actionToggleAddBasketModal())}
      />
      }
    </ViewProvider>
  )
};

export default App;




  // useEffect(() => {
  //   const syncState = () => {
  //     const updatedProducts = [...products];
  //     const updatedFavoritedList = [...favoritedList];

  //     updatedProducts.forEach(product => {
  //       const basketItem = basketList.find(item => item.article === product.article);
  //       const favoritedItem = favoritedList.find(item => item.article === product.article);

  //       product.countInBasket = basketItem ? basketItem.countInBasket : 0;
  //       product.isFavorited = !!favoritedItem;
  //     });

  //     updatedFavoritedList.forEach(favoritedItem => {
  //       const basketItem = basketList.find(item => item.article === favoritedItem.article);
  //       favoritedItem.countInBasket = basketItem ? basketItem.countInBasket : 0;
  //     });

  //     dispatch(actionUpdateProducts(updatedProducts));
  //     dispatch(actionSyncProductsAndFavorited(updatedFavoritedList));
  //   };

  //   syncState();
  // }, [basketList, favoritedList, dispatch, products]);


// import React, { useState, useEffect } from 'react';
// import { Routes, Route } from 'react-router-dom';
// // import Header from "./components/Header/Header.jsx";
// // import ModalAddBasket from './components/ModalAddBasket/ModalAddBasket.jsx';
// // import ModalDeleteFromBasket from './components/ModalDeleteFromBasket/ModalDeleteFromBasket.jsx';
// // import ModalImgZoom from './components/ModalImgZoom/ModalImgZoom.jsx';
// import HomePage from './pages/HomePage/HomePage.jsx';
// // import FavoritePage from './pages/FavoritePage/FavoritePage.jsx';
// // import BasketPage from './pages/BasketPage/BasketPage.jsx';
// // import NotFoundPage from './pages/NotFoundPage/NotFoundPage.jsx';
// // import { selectorProducts, selectorModalContent, selectorFavoritedList, selectorBasketList } from "./store/selectors.js";
// // import { actionProducts, actionModalContent, actionFavoritedList, actionBasketList, actionFetchProducts } from "./store/slices/slice.js";
// import { useDispatch, useSelector } from "react-redux";
// import { actionFetchProducts } from "./store/slices/slice.js";

// const App = () => {
//   const dispatch = useDispatch();
//   const products = useSelector(state => state.products);

//   // const [products, setProducts] = useState([]);
//   // const [products, setProducts] = useState(() => {
//   //   const storedMainProductList = localStorage.getItem('mainProductList');
//   //   return storedMainProductList ? JSON.parse(storedMainProductList) : [];
//   // });
//   // const [isImgZoomModal, setIsImgZoomModal] = useState(false);
//   // const [isAddBasketModal, setIsAddBasketModal] = useState(false);
//   // const [isModalDeleteFromBasket, setIsModalDeleteFromBasket] = useState(false);
//   // const [modalContent, setModalContent] = useState({
//   //   imgCard: undefined,
//   //   modalText: undefined,
//   //   nameCard: undefined,
//   //   price: undefined,
//   //   article: undefined,
//   //   color: undefined,
//   //   isFavorited: undefined,
//   //   countInBasket: undefined,
//   // });
//   // const [favoritedList, setFavoritedList] = useState([]);
//   // const [basketList, setBasketList] = useState([]);
//   // const [isBasketPage, setIsBasketPage] = useState(false);
//   // const [selectedProductInBasket, setSelectedProductInBasket] = useState(null); // вибраний товар у кошику

//   useEffect(() => {
//     dispatch(actionFetchProducts());
//   }, [dispatch]);

//   // useEffect(() => {
//   //   // Якщо products порожній, виконуємо запит на отримання даних з файлу
//   //   if (products.length === 0) {
//   //     fetch("../asets/products.json")
//   //       .then(response => response.json())
//   //       .then(data => {
//   //         setProducts(data);
//   //         // Зберігаємо отримані дані в localStorage
//   //         localStorage.setItem('mainProductList', JSON.stringify(data));
//   //       })
//   //       .catch(error => console.error("Error fetching products: ", error));
//   //   }
//   // }, [products]);

//   // const handleImgZoomModal = () => {
//   //   setIsImgZoomModal((prevState) => !prevState);
//   // }

//   // const handleAddBasketModal = () => {
//   //   setIsAddBasketModal((prevState) => !prevState);
//   // }

//   // const handleModalDeleteFromBasket = (product) => {
//   //   // console.log("product in APP in handleModalDeleteFromBasket", product);
//   //   setSelectedProductInBasket(product); // Встановлюємо вибраний товар у кошику
//   //   // console.log(product.nameCard);
//   //   setIsModalDeleteFromBasket((prevState) => !prevState);
//   // }

//   // const updateFavoriteCountInLocalStorage = (favoritedList) => {
//   //   localStorage.setItem('favoritedList', favoritedList.toString());
//   // }; // оновлення localStorage при зміні кількості товарів у вибраному

//   // const updateBasketCountInLocalStorage = (basketList) => {
//   //   localStorage.setItem('basketList', basketList.toString());
//   // }; // оновлення localStorage при зміні кількості товарів у кошику

//   // const updateMainProductListInLocalStorage = (products) => {
//   //   localStorage.setItem('mainProductList', JSON.stringify(products));
//   // }; // оновлення localStorage при зміні головного списку товарів

//   // // Функція по кліку на зірочку 1).додає/видаляє товар в масиві обраних, 2).в масиві локал сторідж, 3).фарбує зірочку на картці
//   // const handleFavoritedList = (item) => {
//   //   setProducts(prevProducts => {
//   //     // Оновлюємо масив всіх товарів, корегуя в клікнутому товарі значення isFavorited на true/false
//   //     const updatedProducts = prevProducts.map(product => {
//   //       if (product.article === item.article) {
//   //         // Переключаємо стан isFavorited true/false
//   //         const isFavorited = !product.isFavorited;
//   //         // Оновлюємо товар зі зміненим значенням isFavorited
//   //         return { ...product, isFavorited };
//   //       }
//   //       return product;
//   //     });

//   //     // Оновлюємо список обраних товарів
//   //     const updatedFavoritedList = updatedProducts.filter(product => product.isFavorited);
//   //     setFavoritedList(updatedFavoritedList);

//   //     // Оновлюємо localStorage з урахуванням зміни у списку обраних товарів
//   //     updateFavoriteCountInLocalStorage(JSON.stringify(updatedFavoritedList));

//   //     // Оновлюємо список товарів у кошику з урахуванням зміни у списку обраних товарів
//   //     const updatedBasketList = basketList.map(item => {
//   //       const foundProduct = updatedProducts.find(product => product.article === item.article);
//   //       if (foundProduct) {
//   //         return { ...item, isFavorited: foundProduct.isFavorited };
//   //       }
//   //       return item;
//   //     });
//   //     setBasketList(updatedBasketList);
//   //     updateBasketCountInLocalStorage(JSON.stringify(updatedBasketList));

//   //     return updatedProducts;
//   //   });
//   // }

//   // const handleBasketList = (item) => {
//   //   const existingItemIndex = basketList.findIndex(basketItem => basketItem.article === item.article);

//   //   if (existingItemIndex !== -1) {
//   //     // findIndex повертає -1, якщо індекс Не знайдено в масиві. Якщо товар вже є у кошику, оновимо його кількість
//   //     const updatedBasketList = basketList.map((basketItem, index) => {
//   //       if (index === existingItemIndex) {
//   //         return { ...basketItem, countInBasket: basketItem.countInBasket + 1 };
//   //       } else {
//   //         return basketItem;
//   //       }
//   //     });

//   //     setBasketList(updatedBasketList);
//   //     updateBasketCountInLocalStorage(JSON.stringify(updatedBasketList));
//   //   } else {
//   //     // Якщо товару немає у кошику, додайте його
//   //     setBasketList((prevState) => {
//   //       const newBasketList = [...prevState, { ...item, countInBasket: item.countInBasket + 1 }];
//   //       updateBasketCountInLocalStorage(JSON.stringify(newBasketList));
//   //       return newBasketList;
//   //     });
//   //   }

//   //   setProducts(prevProducts => {
//   //     return prevProducts.map(product => {
//   //       if (product.article === item.article) {
//   //         return { ...product, countInBasket: product.countInBasket + 1 };
//   //       } else {
//   //         return product;
//   //       }
//   //     });
//   //   });

//   //   // // Оновлюємо список товарів у списку обраних товарів з урахуванням зміни у кошику
//   //   const updatedFavoritedList = favoritedList.map(favoritedItem => {
//   //     const foundProduct = products.find(product => product.article === favoritedItem.article);
//   //     if (foundProduct) {
//   //       return { ...favoritedItem, countInBasket: foundProduct.countInBasket };
//   //     }
//   //     return favoritedItem;
//   //   });
//   //   setFavoritedList(updatedFavoritedList);
//   //   updateFavoriteCountInLocalStorage(JSON.stringify(updatedFavoritedList));
//   // };

//   // // // Варіант видалення з кошику через модалку
//   // const handleRemoveFromBasket = (itemToRemoveArticle) => {
//   //   // console.log("itemToRemove in APP", itemToRemoveArticle);
//   //   setBasketList(prevState => {
//   //     // Фільтруємо масив товарів у кошику, залишаючи лише ті товари, які не дорівнюють itemToRemove
//   //     const updatedBasketList = prevState.filter(item => item.article !== itemToRemoveArticle);

//   //     // Оновлюємо localStorage з урахуванням нового списку товарів у кошику
//   //     updateBasketCountInLocalStorage(JSON.stringify(updatedBasketList));

//   //     return updatedBasketList;
//   //   });

//   //   setProducts(prevProducts => {
//   //     return prevProducts.map(product => {
//   //       if (product.article === itemToRemoveArticle) {
//   //         return { ...product, countInBasket: 0 };
//   //       } else {
//   //         return product;
//   //       }
//   //     });
//   //   });
//   //   handleModalDeleteFromBasket();
//   //   // Після видалення товару з кошика, оновлюємо контекст для модального вікна
//   //   setSelectedProductInBasket(null); // Або можна передати пустий об'єкт, якщо це необхідно

//   // };

//   // useEffect(() => {
//   //   // Перевірка, чи є дані в localStorage для кошика
//   //   const storedBasket = localStorage.getItem('basketList');
//   //   if (storedBasket) {
//   //     setBasketList(JSON.parse(storedBasket));
//   //   }

//   //   // Перевірка, чи є дані в localStorage для списку обраних товарів
//   //   const storedFavorited = localStorage.getItem('favoritedList');
//   //   if (storedFavorited) {
//   //     setFavoritedList(JSON.parse(storedFavorited));
//   //   }

//   //   // Перевірка, чи є дані в localStorage для головного списку товарів
//   //   const storedMainProductList = localStorage.getItem('mainProductList');
//   //   if (storedMainProductList) {
//   //     setProducts(JSON.parse(storedMainProductList));
//   //   }
//   // }, []);

//   // // Оновлюємо localStorage головного списку товарів на основі поточних змін у головному списку товарів
//   // useEffect(() => {
//   //   updateMainProductListInLocalStorage(products);
//   // }, [products]);

//   // // useEffect(() => {
//   //   // Оновлюємо список товарів у списку обраних товарів з урахуванням зміни у кошику
//   //   const updatedFavoritedList = favoritedList.map(favoritedItem => {
//   //     const foundProduct = basketList.find(product => product.article === favoritedItem.article);
//   //     if (foundProduct) {
//   //       return { ...favoritedItem, countInBasket: foundProduct.countInBasket };
//   //     }
//   //     return favoritedItem;
//   //   });
//   //   setFavoritedList(updatedFavoritedList);
//   //   updateFavoriteCountInLocalStorage(JSON.stringify(updatedFavoritedList));
//   // }, [basketList]);

//   return (
//     <>
//       {/* <Header favoritedList={favoritedList} basketList={basketList} /> */}
//       {/* <Header /> */}
//       <main>
//         <Routes>
//           <Route path="/" element={<HomePage products={products} />} />
//           {/* <Route path="/" element={<HomePage
//             products={products}
//             handleImgZoomModal={handleImgZoomModal}
//             handleFavoritedList={handleFavoritedList}
//             handleAddBasketModal={handleAddBasketModal}
//             setModalContent={setModalContent}
//           />} /> */}
//           {/* <Route path="/favoritepage" element={<FavoritePage
//             favoritedList={favoritedList}
//             handleImgZoomModal={handleImgZoomModal}
//             handleFavoritedList={handleFavoritedList}
//             handleAddBasketModal={handleAddBasketModal}
//             setModalContent={setModalContent}
//           />} /> */}
//           {/* <Route path="/basketpage" element={<BasketPage
//             basketList={basketList}
//             handleImgZoomModal={handleImgZoomModal}
//             handleFavoritedList={handleFavoritedList}
//             handleAddBasketModal={handleAddBasketModal}
//             setModalContent={setModalContent}
//             handleRemoveFromBasket={handleRemoveFromBasket}
//             handleModalDeleteFromBasket={handleModalDeleteFromBasket}
//             isBasketPage={true}
//           />} /> */}
//           {/* <Route path="/notfoundpage" element={<NotFoundPage />} /> */}
//         </Routes>
//       </main>

//       {/* {isImgZoomModal && <ModalImgZoom
//         close={handleImgZoomModal}
//         imgCard={modalContent.imgCard}
//         nameCard={modalContent.nameCard}
//       />} */}

//       {/* {isAddBasketModal && <ModalAddBasket
//         close={handleAddBasketModal}
//         handleBasketList={handleBasketList}
//         imgCard={modalContent.imgCard}
//         modalText={modalContent.modalText}
//         nameCard={modalContent.nameCard}
//         color={modalContent.color}
//         article={modalContent.article}
//         price={modalContent.price}
//         isFavorited={modalContent.isFavorited}
//         countInBasket={modalContent.countInBasket}
//       />} */}

//       {/* {isModalDeleteFromBasket && <ModalDeleteFromBasket
//         close={handleModalDeleteFromBasket}
//         handleRemoveFromBasket={handleRemoveFromBasket}
//         product={selectedProductInBasket}
//       />} */}
//     </>
//   )
// };

// export default App;






// // Варіант видалення з кошику через хрестик напряму, реалізован в іншій гілці
// const handleRemoveFromBasket = (itemToRemove) => {
//   console.log("itemToRemove in APP", itemToRemove);
//   setBasketList(prevState => {
//     // Фільтруємо масив товарів у кошику, залишаючи лише ті товари, які не дорівнюють itemToRemove
//     const updatedBasketList = prevState.filter(item => item.article !== itemToRemove.article);

//     // Оновлюємо localStorage з урахуванням нового списку товарів у кошику
//     updateBasketCountInLocalStorage(JSON.stringify(updatedBasketList));

//     return updatedBasketList;
//   });

//   setProducts(prevProducts => {
//     return prevProducts.map(product => {
//       if (product.article === itemToRemove.article) {
//         return { ...product, countInBasket: 0 };
//       } else {
//         return product;
//       }
//     });
//   });
// };


// // Ця частина коду дозволяє синхронізувати оновлення списку обраних товарів
// // при будь-яких змінах на головній сторінці і в кошику,
// // але ламає список обраних при перевантаженні сторінки потребує доработки
// useEffect(() => {
//   // Оновлюємо список товарів у списку обраних товарів з урахуванням зміни у кошику
//   const updatedFavoritedList = favoritedList.map(favoritedItem => {
//     const foundProduct = products.find(product => product.article === favoritedItem.article);
//     if (foundProduct) {
//       return { ...favoritedItem, countInBasket: foundProduct.countInBasket };
//     }
//     return favoritedItem;
//   });
//   setFavoritedList(updatedFavoritedList);
// updateFavoriteCountInLocalStorage(JSON.stringify(updatedFavoritedList));
// }, [basketList, products]);